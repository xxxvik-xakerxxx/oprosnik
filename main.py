import sys
from PyQt5 import QtWidgets

from PyQt5.QtWidgets import *


import sqlite3

import src.login
import src.program
import src.register

from src.program import CreateOneVopross


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle('MainWindow')

    def showLogin(self):
        self.w1 = src.login.LoginWindow()
        self.w1.loginBtn.clicked.connect(self.LoginCorect)
        self.w1.registerBtn.clicked.connect(self.showRegister)
        self.w1.exit.clicked.connect(sys.exit)

        self.w1.show()
        self.BaseStart()

    def LoginCorect(self):
        loginVvod = str(self.w1.lineEdit.text())
        passVvod = str(self.w1.lineEdit_2.text())
        conn = sqlite3.connect('base/base.db')
        cursor = conn.cursor()
        quer = ("""SELECT name, pass, prop FROM user WHERE name == (?)""")
        cursor.execute(quer, [loginVvod])
        rows = cursor.fetchall()
        if rows == []:
            print("Not user")
        else:

            print(rows[0][1])

            if (loginVvod == rows[0][0] and passVvod == rows[0][1]):
                self.showProgram(rows)
                self.w1.close()
            else:
                print('login Eroor')

    def BaseStart(self):
        try:
            file = open('base/base.db')
        except IOError as e:
            print('Error Open db')
            self.showRegister()
            self.w3.RegisterAdmin.show()
            self.w3.RegisterBt.hide()
            self.w3.loginBt.hide()
            self.w1.close()
            conn = sqlite3.connect("base/base.db")
            cursor = conn.cursor()
            cursor.execute("""CREATE TABLE user (name text, pass text,
            prop integer )""")
            conn.commit()
            conn.close()
            self.w3.RegisterAdmin.clicked.connect(lambda: self.RegUser(1))
            self.w3.RegisterAdmin.clicked.connect(lambda: self.ExitProgram(2))

    def RegUser(self, adminn):
        conn = sqlite3.connect("base/base.db")
        cursor = conn.cursor()
        if adminn == 1:
            admin_mark = 1
        else:
            admin_mark = 0

        CreateUserLogin = str(self.w3.lineEdit.text())
        CreateUserPass = str(self.w3.lineEdit_2.text())
        CreateUserPassRepeat = str(self.w3.lineEdit_3.text())

        quer = ("""SELECT name, pass, prop FROM user WHERE name == (?)""")
        cursor.execute(quer, [CreateUserLogin])
        rows = cursor.fetchall()
        if rows == []:
            if CreateUserPass == CreateUserPassRepeat:
                query = (" INSERT INTO user VALUES (?, ?, ?)")
                conn.execute(query, (CreateUserLogin, CreateUserPass, admin_mark))
                conn.commit()
                conn.close()
                self.ExitProgram(2)
            else:
                print('no correct pass')
        else:
            BaseSearch = rows[0][0]
            print(BaseSearch)
            if BaseSearch == CreateUserLogin:
                print('User login error base')
                self.ExitProgram(2)
            else:
                if CreateUserPass == CreateUserPassRepeat:
                    query = (" INSERT INTO user VALUES (?, ?, ?)")
                    conn.execute(query, (CreateUserLogin, CreateUserPass, admin_mark))
                    conn.commit()
                    conn.close()
                    self.ExitProgram(2)
                else:
                    print('no correct pass')

    def showProgram(self, rows):
        self.w2 = src.program.MainWindow()
        if rows[0][2] == 1:
            print("Admin")
        else:
            self.w2.admin.hide()

        username = rows[0][0]
        userpass = rows[0][1]
        userprop = rows[0][2]

        self.w2.UserWidgetForm.label.setText(username)
        self.w2.UserWidgetForm.DeleteUser.clicked.connect(lambda: self.DeleteUser(username))
        self.w2.UserWidgetForm.ExitUserButon.clicked.connect(lambda: self.ExitProgram(1))
        self.w2.UserWidgetForm.SetPass.clicked.connect(self.w2.NewPassButton)

        self.w2.OprosWidgetForm.CreateOpros.clicked.connect(self.CreateOpros)
        self.w2.OprosWidgetForm.StartOpros.clicked.connect(self.StartOpros)

        self.w2.NewPassDialogs.CloseBtn.clicked.connect(self.w2.userWiew)
        self.w2.NewPassDialogs.OKbtn.clicked.connect(self.PassReset)

        self.ChekAdmin(userprop)

        self.w2.SettingsWidgetForm.CreateOprosBt.clicked.connect(self.CreateOpros)
        self.w2.SettingsWidgetForm.StatOprosBt.clicked.connect(self.OprosStatistic)

        self.w2.exit.clicked.connect(lambda: self.ExitProgram(1))

        self.w2.show()

    def PassReset(self):

        oldPass = str(self.w2.NewPassDialogs.OldPass.text())
        newPass = str(self.w2.NewPassDialogs.NewPass.text())
        newPassProp = str(self.w2.NewPassDialogs.NewPassRep.text())

        conn = sqlite3.connect("base/base.db")
        cursor = conn.cursor()
        quer = ("""SELECT pass FROM user WHERE pass == (?)""")
        cursor.execute(quer, [oldPass])
        rows = cursor.fetchall()

        if rows == []:
            print("No correct Old pass")
        else:
            if newPass != newPassProp:
                print("Nev Pass no correct")
            else:
                quer = ("""UPDATE user SET pass = (?) WHERE pass == (?)""")
                cursor.execute(quer, (newPass, oldPass))
                conn.commit()
                conn.close()
        self.w2.userWiew()

    def CreateOpros(self):
        self.w2.HideWidgets()
        self.w2.CReOprBase.OpisanieOpross.show()
        self.w2.CReOprBase.StartOpross.show()
        self.w2.CReOprBase.lineEdit.show()
        self.w2.CReOprBase.OprossName_2.hide()
        self.w2.CReOprBase.show()
        self.w2.CReOprBase.StartOpross.clicked.connect(self.VoprossCreate)
        self.VoprossCount = 0
        self.w2.CReOprBase.NextVopross.clicked.connect(self.VoprossiWrite)
        self.w2.CReOprBase.EndCreateopross.clicked.connect(self.w2.HideWidgets)

    def VoprossCreate(self):
        self.OprosName = self.w2.CReOprBase.lineEdit.text()
        self.OprosOpis = self.w2.CReOprBase.OpisanieOpross.toPlainText()
        self.OprosTriget = 0
        self.OprosColVopr = 0
        conn = sqlite3.connect("base/base.db")
        cursor = conn.cursor()
        cursor.execute("""SELECT name FROM sqlite_master WHERE type='table' AND name='oprosi'""")
        Chek = cursor.fetchall()

        if Chek == []:
            cursor.execute("""CREATE TABLE oprosi (name text, tab_name text, opisanie text,
            colvop integer )""")
            query = (" INSERT INTO oprosi VALUES (?, ?, ?, ?)")
            conn.execute(query, (self.OprosName, self.OprosTriget, self.OprosOpis, self.OprosColVopr))
            conn.commit()
            conn.close()
        else:
            query = (" INSERT INTO oprosi VALUES (?, ?, ?, ?)")
            conn.execute(query, (self.OprosName, self.OprosTriget, self.OprosOpis, self.OprosColVopr))
            conn.commit()
            conn.close()

        self.w2.CReOprBase.OprossName_2.setText(self.OprosName)

        self.w2.CReOprBase.OpisanieOpross.hide()
        self.w2.CReOprBase.StartOpross.hide()
        self.w2.CReOprBase.lineEdit.hide()
        self.w2.CReOprBase.OprossName_2.show()
        self.w2.CreateVoprossiBase.show()
        self.allVoprosi = []
        self.w2.CreateVoprossiBase.AddVopross.clicked.connect(self.AddOneVopross)

    def AddOneVopross(self):
        self.colvoVoprossov = int(self.w2.CreateVoprossiBase.lineEdit.text())

        if self.colvoVoprossov == 0:
            print("Udalit nechego")

        else:
            self.CleanVoprossi()
            for i in range(self.colvoVoprossov):
                self.Vopros = CreateOneVopross()
                self.Vopros.setObjectName('Vopross_' + str(i))
                self.w2.CreateVoprossiBase.verticalLayout_2.addWidget(self.Vopros)
                self.Vopros.show()
                self.allVoprosi.append(self.Vopros)

    def CleanVoprossi(self):

        if self.allVoprosi == []:
            print("Net voprossof")

        else:
            for i in range(len(self.allVoprosi)):
                self.Voprossi = self.allVoprosi[i]
                self.Voprossi.hide()
        self.allVoprosi = []

    def VoprossiWrite(self):
        self.w2.CreateVoprossiBase.textEdit.clear()
        self.w2.CreateVoprossiBase.lineEdit.clear()
        NameVoprossTable = str(self.w2.CReOprBase.OprossName_2.text())
        VoprossOpis = str(self.w2.CreateVoprossiBase.textEdit.toPlainText())
        conn = sqlite3.connect("base/base.db")
        cursor = conn.cursor()
        query = "SELECT name FROM sqlite_master WHERE type='table' AND name='" + NameVoprossTable + "';"
        cursor.execute(query)
        Chek = cursor.fetchone()
        voprossOne1 = []
        voprossOne = []
        voprossOne1.append(VoprossOpis)

        if Chek == None:
            query = ("""CREATE TABLE {table} (vopross text, otvet text)""")
            conn.execute(query.format(table=NameVoprossTable))
            conn.commit()

            for i in range(len(self.allVoprosi)):
                Voprossi = self.allVoprosi[i]
                VoprossText = self.Voprossi.VoprossText.toPlainText()
                VoprosBall = self.Voprossi.Ball.text()
                PravOtvet = 0

                if self.Voprossi.OtvetSelect.isChecked():
                    PravOtvet = 1

                else:
                    PravOtvet = 0
                Voprossi.hide()
                voprossOne.append(VoprossText)
                voprossOne.append(VoprosBall)
                voprossOne.append(PravOtvet)
                voproseStroka = ' '.join(str(e) for e in voprossOne)
            voprossOne1.append(voproseStroka)
            query = (" INSERT INTO {table} VALUES (?, ?)")
            conn.execute(query.format(table=NameVoprossTable), voprossOne1)
            conn.commit()
            conn.close()
            self.VoprossCount += 1
            self.w2.CReOprBase.VoprossNamber.setText(str(self.VoprossCount))

        else:

            for i in range(len(self.allVoprosi)):
                Voprossi = self.allVoprosi[i]
                VoprossText = self.Voprossi.VoprossText.toPlainText()
                VoprosBall = self.Voprossi.Ball.text()
                PravOtvet = 0

                if self.Voprossi.OtvetSelect.isChecked():
                    PravOtvet = 1

                else:
                    PravOtvet = 0
                Voprossi.hide()
                voprossOne.append(VoprossText)
                voprossOne.append(VoprosBall)
                voprossOne.append(PravOtvet)
                voproseStroka = ' '.join(str(e) for e in voprossOne)
            voprossOne1.append(voproseStroka)
            query = (" INSERT INTO {table} VALUES (?, ?)")
            conn.execute(query.format(table=NameVoprossTable), voprossOne1)
            conn.commit()
            conn.close()
            self.VoprossCount += 1
            self.w2.CReOprBase.VoprossNamber.setText(str(self.VoprossCount))

    def StartOpros(self):
        print("Start Opros")

    def ChekAdmin(self, admin):

        if admin == 0:
            print("Ne Admin")

        else:
            self.w2.AdminWidgetForm.CreateOpros.clicked.connect(self.CreateOpros)
            self.w2.AdminWidgetForm.OprosSetings.clicked.connect(self.OprosSettings)
            self.w2.AdminWidgetForm.OprosStat.clicked.connect(self.OprosStatistic)
            self.w2.AdminWidgetForm.UserStats.clicked.connect(self.UserStatistic)
            self.w2.AdminWidgetForm.CreateUserBT.clicked.connect(self.showRegister)
            self.w2.AdminWidgetForm.UserPropBt.clicked.connect(self.UserPropertis)

    def OprosSettings(self):
        print("OprosSetings")

    def OprosStatistic(self):
        print("OprosStatistic")

    def UserStatistic(self):
        print("UserStat")

    def UserPropertis(self):
        print("UserProp")

    def DeleteUser(self, username):
        text, ok = QInputDialog.getText(self, "Delete user?", "yes?")

        if ok:

            if text == 'yes':
                print("Delete")
                conn = sqlite3.connect("base/base.db")
                cursor = conn.cursor()
                quer = ("""DELETE FROM user WHERE name == (?)""")
                cursor.execute(quer, [username])
                conn.commit()
                conn.close()
                self.ExitProgram(1)

    def showRegister(self):
        self.w3 = src.register.RegisterWindow()
        self.w3.show()
        self.w1.close()
        self.w3.loginBt.show()
        self.w3.RegisterBt.show()
        self.w3.loginBt.clicked.connect(lambda: self.ExitProgram(2))
        self.w3.RegisterAdmin.hide()
        self.w3.RegisterBt.clicked.connect(lambda: self.RegUser(2))

    def ExitProgram(self, namber):
        self.showLogin()

        if namber == 1:
            self.w2.close()

        elif namber == 2:
            self.w3.close()

        else:
            self.w1.close()


def main():
    app1 = QtWidgets.QApplication(sys.argv)
    window1 = MainWindow()
    window1.showLogin()
    app1.exec_()


if __name__ == '__main__':
    main()
