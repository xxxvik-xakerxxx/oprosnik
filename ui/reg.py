# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'reg.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_RegisterUser(object):
    def setupUi(self, RegisterUser):
        RegisterUser.setObjectName("RegisterUser")
        RegisterUser.resize(292, 231)
        self.centralwidget = QtWidgets.QWidget(RegisterUser)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setObjectName("lineEdit")
        self.verticalLayout.addWidget(self.lineEdit)
        self.lineEdit_2 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.verticalLayout.addWidget(self.lineEdit_2)
        self.lineEdit_3 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.verticalLayout.addWidget(self.lineEdit_3)
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.loginBt = QtWidgets.QPushButton(self.centralwidget)
        self.loginBt.setObjectName("loginBt")
        self.horizontalLayout.addWidget(self.loginBt)
        self.RegisterBt = QtWidgets.QPushButton(self.centralwidget)
        self.RegisterBt.setObjectName("RegisterBt")
        self.horizontalLayout.addWidget(self.RegisterBt)
        self.RegisterAdmin = QtWidgets.QPushButton(self.centralwidget)
        self.RegisterAdmin.setObjectName("RegisterAdmin")
        self.horizontalLayout.addWidget(self.RegisterAdmin)
        self.verticalLayout.addLayout(self.horizontalLayout)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        RegisterUser.setCentralWidget(self.centralwidget)

        self.retranslateUi(RegisterUser)
        QtCore.QMetaObject.connectSlotsByName(RegisterUser)

    def retranslateUi(self, RegisterUser):
        _translate = QtCore.QCoreApplication.translate
        RegisterUser.setWindowTitle(_translate("RegisterUser", "RegisterUser"))
        self.lineEdit.setPlaceholderText(_translate("RegisterUser", "Login"))
        self.lineEdit_2.setPlaceholderText(_translate("RegisterUser", "Pass"))
        self.lineEdit_3.setPlaceholderText(_translate("RegisterUser", "PassRepeat"))
        self.loginBt.setText(_translate("RegisterUser", "Login"))
        self.RegisterBt.setText(_translate("RegisterUser", "Register"))
        self.RegisterAdmin.setText(_translate("RegisterUser", "Register Admin"))
