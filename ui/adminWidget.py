# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'adminWidget.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_AdminWidget(object):
    def setupUi(self, AdminWidget):
        AdminWidget.setObjectName("AdminWidget")
        AdminWidget.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(AdminWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.CreateOpros = QtWidgets.QPushButton(AdminWidget)
        self.CreateOpros.setObjectName("CreateOpros")
        self.horizontalLayout.addWidget(self.CreateOpros)
        self.OprosSetings = QtWidgets.QPushButton(AdminWidget)
        self.OprosSetings.setObjectName("OprosSetings")
        self.horizontalLayout.addWidget(self.OprosSetings)
        self.OprosStat = QtWidgets.QPushButton(AdminWidget)
        self.OprosStat.setObjectName("OprosStat")
        self.horizontalLayout.addWidget(self.OprosStat)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.UserStats = QtWidgets.QPushButton(AdminWidget)
        self.UserStats.setObjectName("UserStats")
        self.horizontalLayout_2.addWidget(self.UserStats)
        self.CreateUserBT = QtWidgets.QPushButton(AdminWidget)
        self.CreateUserBT.setObjectName("CreateUserBT")
        self.horizontalLayout_2.addWidget(self.CreateUserBT)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.UserPropBt = QtWidgets.QPushButton(AdminWidget)
        self.UserPropBt.setObjectName("UserPropBt")
        self.verticalLayout.addWidget(self.UserPropBt)

        self.retranslateUi(AdminWidget)
        QtCore.QMetaObject.connectSlotsByName(AdminWidget)

    def retranslateUi(self, AdminWidget):
        _translate = QtCore.QCoreApplication.translate
        AdminWidget.setWindowTitle(_translate("AdminWidget", "AdminWidget"))
        self.CreateOpros.setText(_translate("AdminWidget", "Create Opros"))
        self.OprosSetings.setText(_translate("AdminWidget", "Settings Opros"))
        self.OprosStat.setText(_translate("AdminWidget", "Opros Statistic"))
        self.UserStats.setText(_translate("AdminWidget", "User Statistic"))
        self.CreateUserBT.setText(_translate("AdminWidget", "Create User"))
        self.UserPropBt.setText(_translate("AdminWidget", "User Prop"))
