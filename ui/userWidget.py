# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'userWidget.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(Form)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.Usericon = QtWidgets.QPushButton(Form)
        self.Usericon.setText("")
        self.Usericon.setObjectName("Usericon")
        self.horizontalLayout.addWidget(self.Usericon)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.line = QtWidgets.QFrame(Form)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.listView = QtWidgets.QListView(Form)
        self.listView.setObjectName("listView")
        self.verticalLayout.addWidget(self.listView)
        self.line_2 = QtWidgets.QFrame(Form)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.verticalLayout.addWidget(self.line_2)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.SetPass = QtWidgets.QPushButton(Form)
        self.SetPass.setObjectName("SetPass")
        self.horizontalLayout_2.addWidget(self.SetPass)
        self.ExitUserButon = QtWidgets.QPushButton(Form)
        self.ExitUserButon.setText("")
        self.ExitUserButon.setObjectName("ExitUserButon")
        self.horizontalLayout_2.addWidget(self.ExitUserButon)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.DeleteUser = QtWidgets.QPushButton(Form)
        self.DeleteUser.setObjectName("DeleteUser")
        self.verticalLayout.addWidget(self.DeleteUser)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "UserTable"))
        self.label.setText(_translate("Form", "Username"))
        self.SetPass.setText(_translate("Form", "SetPassword"))
        self.DeleteUser.setText(_translate("Form", "DeleteUser"))
