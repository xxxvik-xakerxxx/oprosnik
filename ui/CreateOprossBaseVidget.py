# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'CreateOprossBaseVidget.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_CreateOprossMain(object):
    def setupUi(self, CreateOprossMain):
        CreateOprossMain.setObjectName("CreateOprossMain")
        CreateOprossMain.resize(389, 259)
        self.verticalLayout = QtWidgets.QVBoxLayout(CreateOprossMain)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.OprossName = QtWidgets.QLabel(CreateOprossMain)
        self.OprossName.setObjectName("OprossName")
        self.horizontalLayout.addWidget(self.OprossName)
        self.lineEdit = QtWidgets.QLineEdit(CreateOprossMain)
        self.lineEdit.setObjectName("lineEdit")
        self.horizontalLayout.addWidget(self.lineEdit)
        self.OprossName_2 = QtWidgets.QLabel(CreateOprossMain)
        self.OprossName_2.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.OprossName_2.setObjectName("OprossName_2")
        self.horizontalLayout.addWidget(self.OprossName_2)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.OpisanieOpross = QtWidgets.QTextEdit(CreateOprossMain)
        self.OpisanieOpross.setObjectName("OpisanieOpross")
        self.verticalLayout.addWidget(self.OpisanieOpross)
        self.StartOpross = QtWidgets.QPushButton(CreateOprossMain)
        self.StartOpross.setObjectName("StartOpross")
        self.verticalLayout.addWidget(self.StartOpross)
        self.line_2 = QtWidgets.QFrame(CreateOprossMain)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.verticalLayout.addWidget(self.line_2)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout.addLayout(self.verticalLayout_2)
        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem2)
        self.line = QtWidgets.QFrame(CreateOprossMain)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(CreateOprossMain)
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.VoprossNamber = QtWidgets.QLabel(CreateOprossMain)
        self.VoprossNamber.setText("")
        self.VoprossNamber.setObjectName("VoprossNamber")
        self.horizontalLayout_2.addWidget(self.VoprossNamber)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem3)
        self.PrevVpross = QtWidgets.QPushButton(CreateOprossMain)
        self.PrevVpross.setObjectName("PrevVpross")
        self.horizontalLayout_2.addWidget(self.PrevVpross)
        self.NextVopross = QtWidgets.QPushButton(CreateOprossMain)
        self.NextVopross.setObjectName("NextVopross")
        self.horizontalLayout_2.addWidget(self.NextVopross)
        self.EndCreateopross = QtWidgets.QPushButton(CreateOprossMain)
        self.EndCreateopross.setObjectName("EndCreateopross")
        self.horizontalLayout_2.addWidget(self.EndCreateopross)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(CreateOprossMain)
        QtCore.QMetaObject.connectSlotsByName(CreateOprossMain)

    def retranslateUi(self, CreateOprossMain):
        _translate = QtCore.QCoreApplication.translate
        CreateOprossMain.setWindowTitle(_translate("CreateOprossMain", "Form"))
        self.OprossName.setText(_translate("CreateOprossMain", "Opross Name"))
        self.OprossName_2.setText(_translate("CreateOprossMain", "OprossName"))
        self.StartOpross.setText(_translate("CreateOprossMain", "Create "))
        self.label.setText(_translate("CreateOprossMain", "Vopross №:"))
        self.PrevVpross.setText(_translate("CreateOprossMain", "Prev Vopross"))
        self.NextVopross.setText(_translate("CreateOprossMain", "Next Vopross"))
        self.EndCreateopross.setText(_translate("CreateOprossMain", "End Create"))
