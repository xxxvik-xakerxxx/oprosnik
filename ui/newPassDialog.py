# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'newPassDialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_PasswordResetDialod(object):
    def setupUi(self, PasswordResetDialod):
        PasswordResetDialod.setObjectName("PasswordResetDialod")
        PasswordResetDialod.resize(184, 138)
        self.verticalLayout = QtWidgets.QVBoxLayout(PasswordResetDialod)
        self.verticalLayout.setObjectName("verticalLayout")
        self.OldPass = QtWidgets.QLineEdit(PasswordResetDialod)
        self.OldPass.setObjectName("OldPass")
        self.verticalLayout.addWidget(self.OldPass)
        self.NewPass = QtWidgets.QLineEdit(PasswordResetDialod)
        self.NewPass.setObjectName("NewPass")
        self.verticalLayout.addWidget(self.NewPass)
        self.NewPassRep = QtWidgets.QLineEdit(PasswordResetDialod)
        self.NewPassRep.setObjectName("NewPassRep")
        self.verticalLayout.addWidget(self.NewPassRep)
        self.line = QtWidgets.QFrame(PasswordResetDialod)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.OKbtn = QtWidgets.QPushButton(PasswordResetDialod)
        self.OKbtn.setObjectName("OKbtn")
        self.horizontalLayout.addWidget(self.OKbtn)
        self.CloseBtn = QtWidgets.QPushButton(PasswordResetDialod)
        self.CloseBtn.setObjectName("CloseBtn")
        self.horizontalLayout.addWidget(self.CloseBtn)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(PasswordResetDialod)
        QtCore.QMetaObject.connectSlotsByName(PasswordResetDialod)

    def retranslateUi(self, PasswordResetDialod):
        _translate = QtCore.QCoreApplication.translate
        PasswordResetDialod.setWindowTitle(_translate("PasswordResetDialod", "ResetPasword"))
        self.OldPass.setPlaceholderText(_translate("PasswordResetDialod", "OldPass"))
        self.NewPass.setPlaceholderText(_translate("PasswordResetDialod", "NewPass"))
        self.NewPassRep.setPlaceholderText(_translate("PasswordResetDialod", "NewPassRep"))
        self.OKbtn.setText(_translate("PasswordResetDialod", "Ok"))
        self.CloseBtn.setText(_translate("PasswordResetDialod", "Close"))
