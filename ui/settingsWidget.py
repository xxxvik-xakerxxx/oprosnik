# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'settingsWidget.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_SetingsWidget(object):
    def setupUi(self, SetingsWidget):
        SetingsWidget.setObjectName("SetingsWidget")
        SetingsWidget.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(SetingsWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.CreateOprosBt = QtWidgets.QPushButton(SetingsWidget)
        self.CreateOprosBt.setObjectName("CreateOprosBt")
        self.verticalLayout.addWidget(self.CreateOprosBt)
        self.StatOprosBt = QtWidgets.QPushButton(SetingsWidget)
        self.StatOprosBt.setObjectName("StatOprosBt")
        self.verticalLayout.addWidget(self.StatOprosBt)

        self.retranslateUi(SetingsWidget)
        QtCore.QMetaObject.connectSlotsByName(SetingsWidget)

    def retranslateUi(self, SetingsWidget):
        _translate = QtCore.QCoreApplication.translate
        SetingsWidget.setWindowTitle(_translate("SetingsWidget", "SetingsWidget"))
        self.CreateOprosBt.setText(_translate("SetingsWidget", "Create Opros"))
        self.StatOprosBt.setText(_translate("SetingsWidget", "Statistic Opros"))
