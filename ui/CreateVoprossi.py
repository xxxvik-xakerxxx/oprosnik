# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'CreateVoprossi.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_CreateOprossVoprosi(object):
    def setupUi(self, CreateOprossVoprosi):
        CreateOprossVoprosi.setObjectName("CreateOprossVoprosi")
        CreateOprossVoprosi.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(CreateOprossVoprosi)
        self.verticalLayout.setObjectName("verticalLayout")
        self.textEdit = QtWidgets.QTextEdit(CreateOprossVoprosi)
        self.textEdit.setObjectName("textEdit")
        self.verticalLayout.addWidget(self.textEdit)
        self.line = QtWidgets.QFrame(CreateOprossVoprosi)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout.addLayout(self.verticalLayout_2)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.line_2 = QtWidgets.QFrame(CreateOprossVoprosi)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.verticalLayout.addWidget(self.line_2)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(CreateOprossVoprosi)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.lineEdit = QtWidgets.QLineEdit(CreateOprossVoprosi)
        self.lineEdit.setObjectName("lineEdit")
        self.horizontalLayout.addWidget(self.lineEdit)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.AddVopross = QtWidgets.QPushButton(CreateOprossVoprosi)
        self.AddVopross.setObjectName("AddVopross")
        self.horizontalLayout.addWidget(self.AddVopross)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(CreateOprossVoprosi)
        QtCore.QMetaObject.connectSlotsByName(CreateOprossVoprosi)

    def retranslateUi(self, CreateOprossVoprosi):
        _translate = QtCore.QCoreApplication.translate
        CreateOprossVoprosi.setWindowTitle(_translate("CreateOprossVoprosi", "Form"))
        self.label.setText(_translate("CreateOprossVoprosi", "ColVooVoprossov"))
        self.lineEdit.setText(_translate("CreateOprossVoprosi", "0"))
        self.AddVopross.setText(_translate("CreateOprossVoprosi", "Add Vopross"))
