# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'untitledCSSeyW.ui'
##
## Created by: Qt User Interface Compiler version 5.14.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.horizontalLayout = QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.user = QPushButton(self.centralwidget)
        self.user.setObjectName(u"user")

        self.verticalLayout_2.addWidget(self.user)

        self.oprosi = QPushButton(self.centralwidget)
        self.oprosi.setObjectName(u"oprosi")

        self.verticalLayout_2.addWidget(self.oprosi)

        self.admin = QPushButton(self.centralwidget)
        self.admin.setObjectName(u"admin")

        self.verticalLayout_2.addWidget(self.admin)

        self.setings = QPushButton(self.centralwidget)
        self.setings.setObjectName(u"setings")

        self.verticalLayout_2.addWidget(self.setings)

        self.exit = QPushButton(self.centralwidget)
        self.exit.setObjectName(u"exit")

        self.verticalLayout_2.addWidget(self.exit)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)


        self.horizontalLayout.addLayout(self.verticalLayout_2)

        self.line = QFrame(self.centralwidget)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.VLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.horizontalLayout.addWidget(self.line)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")

        self.horizontalLayout.addLayout(self.verticalLayout)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.user.setText(QCoreApplication.translate("MainWindow", u"User", None))
        self.oprosi.setText(QCoreApplication.translate("MainWindow", u"Oprosi", None))
        self.admin.setText(QCoreApplication.translate("MainWindow", u"Admin", None))
        self.setings.setText(QCoreApplication.translate("MainWindow", u"setings", None))
        self.exit.setText(QCoreApplication.translate("MainWindow", u"exit", None))
    # retranslateUi

