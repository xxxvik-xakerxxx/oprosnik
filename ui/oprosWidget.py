# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'oprosWidget.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_OprossWidget(object):
    def setupUi(self, OprossWidget):
        OprossWidget.setObjectName("OprossWidget")
        OprossWidget.resize(400, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(OprossWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.CreateOpros = QtWidgets.QPushButton(OprossWidget)
        self.CreateOpros.setObjectName("CreateOpros")
        self.horizontalLayout.addWidget(self.CreateOpros)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.scrollArea = QtWidgets.QScrollArea(OprossWidget)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 378, 216))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.horizontalLayout_2.addWidget(self.scrollArea)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.OprosTeme = QtWidgets.QComboBox(OprossWidget)
        self.OprosTeme.setCurrentText("")
        self.OprosTeme.setObjectName("OprosTeme")
        self.horizontalLayout_3.addWidget(self.OprosTeme)
        self.StartOpros = QtWidgets.QPushButton(OprossWidget)
        self.StartOpros.setObjectName("StartOpros")
        self.horizontalLayout_3.addWidget(self.StartOpros)
        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.retranslateUi(OprossWidget)
        QtCore.QMetaObject.connectSlotsByName(OprossWidget)

    def retranslateUi(self, OprossWidget):
        _translate = QtCore.QCoreApplication.translate
        OprossWidget.setWindowTitle(_translate("OprossWidget", "OprosWidget"))
        self.CreateOpros.setText(_translate("OprossWidget", "Create Opros"))
        self.StartOpros.setText(_translate("OprossWidget", "Start Opros"))
