# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'VoprossBase1ui.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(399, 88)
        self.horizontalLayout = QtWidgets.QHBoxLayout(Form)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.OtvetSelect = QtWidgets.QRadioButton(Form)
        self.OtvetSelect.setText("")
        self.OtvetSelect.setObjectName("OtvetSelect")
        self.horizontalLayout.addWidget(self.OtvetSelect)
        self.VoprossText = QtWidgets.QTextEdit(Form)
        self.VoprossText.setObjectName("VoprossText")
        self.horizontalLayout.addWidget(self.VoprossText)
        self.Ball = QtWidgets.QLineEdit(Form)
        self.Ball.setObjectName("Ball")
        self.horizontalLayout.addWidget(self.Ball)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
