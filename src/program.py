import sys
from PyQt5 import QtWidgets
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

import ui.stat
import ui.userWidget
import ui.oprosWidget
import ui.adminWidget
import ui.settingsWidget
import ui.CreateOprossBaseVidget
import ui.CreateVoprossi
import ui.VoprosBase1

import ui.newPassDialog


class MainWindow(QtWidgets.QMainWindow, ui.stat.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self.user.setIcon(QIcon('ui/user.png'))
        self.user.setIconSize(QSize(75,75))
        self.user.clicked.connect(self.userWiew)
        self.UserWidgetForm = UserWidget()
        self.verticalLayout.addWidget(self.UserWidgetForm)
        self.UserWidgetForm.hide()

        self.oprosi.setIcon(QIcon('ui/opros.png'))
        self.oprosi.setIconSize(QSize(75,75))
        self.oprosi.clicked.connect(self.OprosButton)
        self.OprosWidgetForm = OprosWidget()
        self.verticalLayout.addWidget(self.OprosWidgetForm)
        self.OprosWidgetForm.hide()

        self.admin.setIcon(QIcon('ui/admin.png'))
        self.admin.setIconSize(QSize(75,75))
        self.admin.clicked.connect(self.AdminButton)
        self.AdminWidgetForm = AdminWidget()
        self.verticalLayout.addWidget(self.AdminWidgetForm)
        self.AdminWidgetForm.hide()

        self.setings.setIcon(QIcon('ui/settings.png'))
        self.setings.setIconSize(QSize(75,75))
        self.setings.clicked.connect(self.SettingsButton)
        self.SettingsWidgetForm = SettingsWidget()
        self.verticalLayout.addWidget(self.SettingsWidgetForm)
        self.SettingsWidgetForm.hide()


        self.NewPassDialogs = NewPassDialog()
        self.verticalLayout.addWidget(self.NewPassDialogs)
        self.NewPassDialogs.hide()

        self.CReOprBase = CreateOprossWidgetBase()
        self.verticalLayout.addWidget(self.CReOprBase)
        self.CReOprBase.hide()

        self.CreateVoprossiBase = CreateVoprossi()
        self.CReOprBase.verticalLayout_2.addWidget(self.CreateVoprossiBase)
        self.CreateVoprossiBase.hide()

        self.exit.setIcon(QIcon('ui/exit.png'))
        self.exit.setIconSize(QSize(75,75))
        #self.exit.clicked.connect(self.ExitButoon)

        self.OprosWEnable = False
        self.UserWEnable = False
        self.AdminWEnable = False
        self.SettingsWEnable = False
        self.NewPassEnable = False

        #self.DefWiew()

    #def ExitButoon(self):
    #    sys.exit()


    def DefWiew(self):
        self.UserWEnable = True
        self.UserWidgetForm = UserWidget()
        self.verticalLayout.addWidget(self.UserWidgetForm)
        self.UserWidgetForm.show()
       
    def HideWidgets(self):
        self.CReOprBase.hide()
        self.CreateVoprossiBase.hide()
       
        if self.OprosWEnable == True:
            self.OprosWEnable = False
            self.OprosWidgetForm.hide()
           
        elif self.AdminWEnable == True:
            self.AdminWEnable = False
            self.AdminWidgetForm.hide()

        elif self.UserWEnable == True:
            self.UserWEnable = False
            self.UserWidgetForm.hide()

        elif self.SettingsWEnable == True:
            self.SettingsWEnable = False
            self.SettingsWidgetForm.hide()

        elif self.NewPassEnable == True:
            self.NewPassEnable = False
            self.NewPassDialogs.hide()
        

    def userWiew(self):
        self.HideWidgets()
        self.UserWidgetForm.show()
        self.UserWEnable = True

    def OprosButton(self):
        self.HideWidgets()
        self.OprosWEnable = True
        self.OprosWidgetForm.show()

    def AdminButton(self):
        self.HideWidgets()
        self.AdminWEnable = True
        self.AdminWidgetForm.show()

    def SettingsButton(self):
        self.HideWidgets()
        self.SettingsWEnable = True
        self.SettingsWidgetForm.show()

    def NewPassButton(self):
        self.HideWidgets()
        self.NewPassEnable = True
        self.NewPassDialogs.show()

class UserWidget(QWidget, ui.userWidget.Ui_Form):
    def __init__(self):
        super(QWidget, self).__init__()
        self.setupUi(self)
        #self.mainexit = main.MainWindow()
        #self.ExitUserButon.clicked.connect(self.mainexit.Cliced_exit)
        self.Usericon.setIcon(QIcon('ui/user.png'))
        self.Usericon.setIconSize(QSize(100,100))
        self.ExitUserButon.setIcon(QIcon('ui/exit.png'))
        self.ExitUserButon.setIconSize(QSize(15,15))

       
        #self.UserWidgetForm = UserWidget()
        #self.verticalLayout.addWidget(self.UserWidgetForm)

class OprosWidget(QWidget, ui.oprosWidget.Ui_OprossWidget):
    def __init__(self):
        super(QWidget, self).__init__()
        self.setupUi(self)


class AdminWidget(QWidget, ui.adminWidget.Ui_AdminWidget):
    def __init__(self):
        super(QWidget, self).__init__()
        self.setupUi(self)
        #self.OprosWidgetForm = OprosWidget()
        #self.verticalLayout.addWidget(self.OprosWidgetForm)

class SettingsWidget(QWidget, ui.settingsWidget.Ui_SetingsWidget):
    def __init__(self):
        super(QWidget, self).__init__()
        self.setupUi(self)


class NewPassDialog(QWidget, ui.newPassDialog.Ui_PasswordResetDialod):
    def __init__(self):
        super(QWidget, self).__init__()
        self.setupUi(self)
        self.OldPass.setEchoMode(QtWidgets.QLineEdit.Password)
        self.NewPass.setEchoMode(QtWidgets.QLineEdit.Password)
        self.NewPassRep.setEchoMode(QtWidgets.QLineEdit.Password)
    #def UserButton(self):
        #userTab = QWidget()
        #self.UserWidget = src.userWidget.Ui_Form()
        #self.UserWidget.setupUi(userTab)
        #self.UserWidget.show()

class CreateOprossWidgetBase(QWidget, ui.CreateOprossBaseVidget.Ui_CreateOprossMain):
    def __init__(self):
        super(QWidget, self).__init__()
        self.setupUi(self)

class CreateVoprossi(QWidget, ui.CreateVoprossi.Ui_CreateOprossVoprosi):
    def __init__(self):
        super(QWidget, self).__init__()
        self.setupUi(self)

class CreateOneVopross(QWidget, ui.VoprosBase1.Ui_Form):
    def __init__(self):
        super(QWidget, self).__init__()
        self.setupUi(self)


def program():
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec_()

if __name__ == '__main__':
    program()
